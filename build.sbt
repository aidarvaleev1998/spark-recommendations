name := "spark-recommendations"

version := "0.1"

scalaVersion := "2.12.10"
val sparkVersion = "3.0.1"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" % "spark-mllib_2.12" % sparkVersion,
  "org.scalanlp" %% "breeze" % "1.1",
)