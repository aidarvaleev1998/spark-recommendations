import scala.collection.Map
import scala.util.Random
import breeze.linalg.{DenseMatrix, DenseVector, InjectNumericOps, sum}
import breeze.numerics.{log, sigmoid}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{IntegerType, StructField, StructType}
import org.apache.spark.sql.SparkSession
import math.max
import math.min

object Train {
  def read_data(path: String, spark: SparkSession): RDD[(Int, Int)] = {
    spark.read.format("csv")
      // the original data is store in CSV format
      // header: source_node, destination_node
      // here we read the data from CSV and export it as RDD[(Int, Int)],
      // i.e. as RDD of edges
      .option("header", "true")
      // State that the header is present in the file
      .schema(StructType(Array(
        StructField("source_node", IntegerType, false),
        StructField("destination_node", IntegerType, false)
      ))) // Define schema of the input data
      .load(path)
      // Read the file as DataFrame
      .rdd.map(row => (row.getAs[Int](0), row.getAs[Int](1)))
    // Interpret DF as RDD
  }

  def create_embedding_matrix(n_samples: Int, n_columns: Int): DenseMatrix[Float] = {
    val normal = breeze.stats.distributions.Gaussian(0, 1)
    DenseMatrix.rand(n_samples, n_columns, normal).map(_.toFloat)
  }

  def edge_probability(src_vec: DenseVector[Float], trt_vec: DenseVector[Float]): Float = {
    sigmoid(min(max(src_vec.dot(trt_vec), -10), 10))
  }

  def estimate_gradients_for_edge(
                                   source: Int,
                                   target: Int,
                                   emb_src_broad: Broadcast[DenseMatrix[Float]],
                                   emb_tgt_broad: Broadcast[DenseMatrix[Float]],
                                   num_nodes: Int,
                                   n_neg_samples: Int,
                                 ): (Float, Array[((Int, DenseVector[Float]), (Int, DenseVector[Float]))]) = {
    val emb_src = emb_src_broad.value // retrieve snapshot of emb_src
    val emb_tgt = emb_tgt_broad.value // retrieve snapshot of emb_tgt

    val src = emb_src(::, source)
    val tgt = emb_tgt(::, target)

    // Generate negative samples
    val neg_sample_tgt = {
      for (_ <- 0 until n_neg_samples) yield Random.nextInt(num_nodes - 1)
    }.map(i => (i, emb_tgt(::, i)))

    // Compute loss
    val neg_sample_probabilities: DenseVector[Float] = DenseVector.apply(
      neg_sample_tgt.map(x => 1f - edge_probability(src, x._2)).toArray
    )
    val loss = - log(edge_probability(src, tgt)) - sum(log(neg_sample_probabilities))

    // Estimate gradients for positive and negative edges
    def tgt_gradient(t: DenseVector[Float], y: Int) = src * (sigmoid(src.dot(t)) - y)
    def src_gradient(t: DenseVector[Float], y: Int) = tgt * (sigmoid(src.dot(t)) - y)

    val result =
      Array(((source, src_gradient(tgt, 1)), (target, tgt_gradient(tgt, 1)))) ++
        neg_sample_tgt.map(x =>
          ((source, src_gradient(x._2, 0)), (x._1, tgt_gradient(x._2, 0)))
        ).toArray[((Int, DenseVector[Float]), (Int, DenseVector[Float]))]
    (loss, result)
  }

  def estimate_gradients(
                          batch: RDD[(Int, Int)],
                          emb_src: Broadcast[DenseMatrix[Float]],
                          emb_tgt: Broadcast[DenseMatrix[Float]],
                          num_nodes: Int,
                          n_neg_samples: Int,
                        ): (Double, Map[Int, DenseVector[Float]], Map[Int, DenseVector[Float]]) = {
    val loss_grads = batch.map( edge =>
      estimate_gradients_for_edge(edge._1, edge._2, emb_src, emb_tgt, num_nodes, n_neg_samples)
    )
    val loss = loss_grads.map(_._1).sum() / batch.count() / n_neg_samples

    val grads = loss_grads.flatMap(_._2)

    val src_grads_local = grads
      .map(_._1) // has non unique keys
      .reduceByKey(_+_) // all keys are unique
      .collectAsMap() // download all the gradients to the driver and convert to HashMap (dictionary)

    val tgt_grads_local = grads
      .map(_._2)
      .reduceByKey(_+_)
      .collectAsMap()

    (loss, src_grads_local, tgt_grads_local)
  }

  def main(args: Array[String]): Unit = {
    // Check input
    if (args.length < 1) {
      println(
        """Wrong amount of arguments. Need to provide:
          |    * path - path to data.
          |""".stripMargin)
      return
    }

    val num_nodes: Int = 40334
    val emb_dim: Int = 50
    val epochs: Int = 10
    val lr: Float = 1000f
    val batch_size: Int = 10000
    val neg_samples: Int = 40

    // Init Spark context
    val conf = new SparkConf()
      .setAppName("Train")

    implicit val sc: SparkContext = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    // Init Spark SQL
    val spark = SparkSession
      .builder()
      .appName("Ranker")
      .getOrCreate()
    import spark.implicits._

    // Read data
    val path = args(0)
    val data = read_data(path, spark).zipWithIndex()

    val num_edges = data.count().toInt

    // model params = two matrices
    val emb_src = create_embedding_matrix(emb_dim, num_nodes)
    val emb_tgt = create_embedding_matrix(emb_dim, num_nodes)

    var emb_src_brd: Broadcast[DenseMatrix[Float]] = sc.broadcast(emb_src)
    var emb_tgt_brd: Broadcast[DenseMatrix[Float]] = sc.broadcast(emb_tgt)

    // Throughout the batches
    println(num_edges)
    for (epoch <- 0 until epochs) {
      for (batch_i <- 1 to num_edges / batch_size) {
        emb_src_brd = sc.broadcast(emb_src)
        emb_tgt_brd = sc.broadcast(emb_tgt)

        val batch = data
          .filter(a => (batch_i - 1) * batch_size <= a._2 && a._2 < (batch_i * batch_size))
          .map(a => a._1)

        val (loss, src_grads, tgt_grads) = estimate_gradients(
          batch, emb_src_brd, emb_tgt_brd, num_nodes, neg_samples
        )

        val scale: Float = lr / batch_size / neg_samples

        for ((k: Int, v: DenseVector[Float]) <- src_grads) {
          emb_src(::, k) := emb_src(::, k) - scale * v
        }

        for ((k: Int, v: DenseVector[Float]) <- tgt_grads) {
          emb_tgt(::, k) := emb_tgt(::, k) - scale * v
        }
        println(s"Epoch $epoch, batch $batch_i, loss $loss")
      }}

    // Save
    val save_path = args(1)
    sc.parallelize(emb_src.data).saveAsTextFile(save_path + "matrix_src")
    sc.parallelize(emb_tgt.data).saveAsTextFile(save_path + "matrix_tgt")
  }
}
