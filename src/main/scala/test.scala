import scala.util.Random
import breeze.linalg.*
import breeze.linalg.{DenseMatrix, DenseVector}
import breeze.numerics.{sigmoid}
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{IntegerType, StructField, StructType}
import org.apache.spark.sql.{SparkSession}

import scala.math.{max, min}

object Test{
  def read_data(path: String, spark: SparkSession): RDD[(Int, Int)] = {
    spark.read.format("csv")
      // the original data is store in CSV format
      // header: source_node, destination_node
      // here we read the data from CSV and export it as RDD[(Int, Int)],
      // i.e. as RDD of edges
      .option("header", "true")
      // State that the header is present in the file
      .schema(StructType(Array(
        StructField("source_node", IntegerType, false),
        StructField("destination_node", IntegerType, false)
      ))) // Define schema of the input data
      .load(path)
      // Read the file as DataFrame
      .rdd.map(row => (row.getAs[Int](0), row.getAs[Int](1)))
    // Interpret DF as RDD
  }
  def edge_probability(src_vec: DenseVector[Float], trt_vec: DenseVector[Float]): Float = {
    sigmoid(min(max(src_vec.dot(trt_vec) / 5, -12), 12))
  }

  def main(args: Array[String]): Unit = {
    // Init Spark context
    val conf = new SparkConf()
      .setAppName("Test")
      .setMaster("local[*]")

    implicit val sc: SparkContext = new SparkContext(conf)
    sc.setLogLevel("ERROR")

    // Init Spark SQL
    val spark = SparkSession
      .builder()
      .appName("Test")
      .getOrCreate()
    import spark.implicits._

    // Path to matrices
    val path_mtr = args(0)
    // Path to train and test data
    val path_train = args(1)
    val path_test = args(2)
    val train_data = read_data(path_train, spark)
    val test_data = read_data(path_test, spark)

    // Hyperparams
    val num_nodes: Int = 40334
    val emb_dim: Int = 50
    // Number of random nodes
    val Q = 100
    // Top of predictions
    val k = 10
    val emb_src = new DenseMatrix(emb_dim, num_nodes, sc.textFile(path_mtr+"//matrix_src//").collect().map(x => x.toFloat))
    val emb_tgt = new DenseMatrix(emb_dim, num_nodes, sc.textFile(path_mtr+"//matrix_tgt//").collect().map(x => x.toFloat))
    val rand_nodes = Random.shuffle(test_data.map(x => x._1).collect().toList).takeRight(Q)
    var MAP = 0.0
    for (i <- 0 until Q) {
      val node = rand_nodes(i)
      val node_rep = emb_src(::,node)
      val neib: Array[Int] = train_data.filter(x => x._1 == node).map(x => x._2).collect()
      val predictions = emb_tgt(::,*).map(x => edge_probability(x,node_rep)).
        t.toArray.zipWithIndex.reverse.sortBy(_._1). // Sort candidates by predicted rating in the descending order
        filterNot(x => (neib contains(x._2)) | (x._2==node)). // Filter out nodes with existing edges from the recommendation
        takeRight(k)// Get top 10 recommendation
      // Relevant nodes
      val test_neib: Array[Int] = test_data.filter(x => x._1 == node).map(x => x._2).collect()
      // Average precision
      val AP = predictions.filter(x => test_neib.contains(x._2)).map(x=>x._1).sum / test_neib.length
      MAP += AP
      println("Node" + node, " correct prediction: " + predictions.filter(x => test_neib.contains(x._2)).toList)
    }
    MAP /= Q
    println("MAP=",MAP)
  }
}
